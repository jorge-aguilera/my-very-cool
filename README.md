## My VeryCool

## Requirements

- Java 11

## Build

`./gradlew build`

## Documentation

`./gradlew asciidoctor`

If build ends ok you can find the documentation at `build/docs/asciidoc/index.html`

You can find last version of the documentation at `https://jorge-aguilera.gitlab.io/my-very-cool`

## Development

Run a mongo instance, i.e.:
  `docker run -d --name mongodb-instance -p 27017:27017 mongo`

Run the application:
 `./gradew run`

Request an access token:
  `http localhost:8080/login username=dummy password=test`

Add and idea:
  `http localhost:8080/ideas Authorization:"Bearer $access_token" text=balbalbal type=note`

  ```
{
    "attributes": {
        "text": "balbalbal"
    },
    "author": {
        "id": "c1",
        "name": "customer 1"
    },
    "id": "611a7561c1b67356547f9751",
    "type": "note"
}
```

Request all ideas for a customer (their own and shared with him)

    `http localhost:8080/ideas Authorization:"Bearer $access_token"`

```
[
    {
        "attributes": {
            "text": "balbalbal"
        },
        "author": {
            "id": "c1",
            "name": "customer 1"
        },
        "id": "611a76a0ccd4ec020639908b",
        "type": "note"
    }
]
```
