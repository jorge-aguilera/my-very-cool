package com.myverycool.core.model;

import io.micronaut.core.annotation.Introspected;

@Introspected
public class Customer {

    public String id;
    public String name;

}
