package com.myverycool.core.model;

import io.micronaut.core.annotation.Introspected;

import java.util.List;
import java.util.Map;

@Introspected
public class Idea {

    public String id;

    public Customer author;

    public String type;

    public List<Customer> shared;

    public Map<String, String> attributes = Map.of();

}
