package com.myverycool.core.usecases;

public abstract class UseCaseBase<UseCaseInput, UseCasePresenter> {

    public abstract void execute(UseCaseInput input, UseCasePresenter presenter);

}
