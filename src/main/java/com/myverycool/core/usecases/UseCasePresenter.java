package com.myverycool.core.usecases;

public interface UseCasePresenter <P>{

    void onSuccess( P result);

    void onError(Throwable t);
}
