package com.myverycool.core.usecases.ideas;

import com.myverycool.core.usecases.UseCasePresenter;

public class SimplePresenter<P> implements UseCasePresenter<P> {

    P result;
    boolean success;

    @Override
    public void onSuccess(P result) {
        this.success = true;
        this.result = result;
    }

    @Override
    public void onError(Throwable t) {
        this.success = false;
    }

    public boolean isSuccess(){
        return success;
    }

    public P getResult(){
        return result;
    }
}
