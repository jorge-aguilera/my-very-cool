package com.myverycool.core.usecases.ideas.add;

import java.util.Map;

public class AddInput {

    private String customerId;
    private String customer;
    private String type;
    private Map<String, String> attributes = Map.of();

    public AddInput(String customerId, String customer, String type, Map<String, String> attributes) {
        this.customerId = customerId;
        this.customer = customer;
        this.type = type;
        this.attributes = attributes;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getCustomer() {
        return customer;
    }

    public String getType() {
        return type;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }
}
