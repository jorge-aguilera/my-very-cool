package com.myverycool.core.usecases.ideas.add;

import com.myverycool.core.model.Customer;
import com.myverycool.core.model.Idea;
import com.myverycool.core.usecases.UseCaseBase;
import com.myverycool.core.usecases.UseCasePresenter;
import com.myverycool.outbound.PersistencePort;

import javax.inject.Singleton;
import java.util.Map;

@Singleton
public class AddUseCase extends UseCaseBase<AddInput, UseCasePresenter<Idea>> {

    PersistencePort persistencePort;

    public AddUseCase(PersistencePort persistencePort) {
        this.persistencePort = persistencePort;
    }

    @Override
    public void execute(AddInput addInput, UseCasePresenter<Idea> itemUseCasePresenter) {
        try {
            validateParams(addInput);
        }catch(IllegalArgumentException illegalArgumentException) {
            itemUseCasePresenter.onError(illegalArgumentException);
            return;
        }
        try{
            Idea idea = fromInput(addInput);
            idea = persistencePort.save(idea);
            itemUseCasePresenter.onSuccess(idea);
        }catch (RuntimeException re){
            itemUseCasePresenter.onError(re);
        }
    }

    Idea fromInput(AddInput addInput){
        Idea idea = new Idea();
        idea.author = new Customer();
        idea.author.id = addInput.getCustomerId();
        idea.author.name = addInput.getCustomer();
        idea.type = addInput.getType();
        idea.attributes = addInput.getAttributes();
        return idea;
    }

    void validateParams(AddInput addInput) throws IllegalArgumentException{
        if( addInput.getCustomerId() == null || addInput.getCustomerId().length() == 0){
            throw new IllegalArgumentException("customer is required");
        }
        if( addInput.getCustomer() == null || addInput.getCustomer().length() == 0){
            throw new IllegalArgumentException("customer is required");
        }
        if( addInput.getAttributes() == null || addInput.getAttributes().size() == 0){
            throw new IllegalArgumentException("Attributes is required");
        }
        if( addInput.getType() == null ){
            throw new IllegalArgumentException("Type is required");
        }
    }
}
