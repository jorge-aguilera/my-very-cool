package com.myverycool.core.usecases.ideas.list;

import com.myverycool.core.usecases.UseCaseInput;

public class ListInput implements UseCaseInput {
    private String customerId;

    public ListInput(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerId() {
        return customerId;
    }

}
