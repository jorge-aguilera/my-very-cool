package com.myverycool.core.usecases.ideas.list;

import com.myverycool.core.model.Idea;
import com.myverycool.core.usecases.UseCaseBase;
import com.myverycool.core.usecases.UseCasePresenter;
import com.myverycool.outbound.PersistencePort;
import io.reactivex.Flowable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.util.List;

@Singleton
public class ListUseCase extends UseCaseBase<ListInput, UseCasePresenter<List<Idea>>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ListUseCase.class);

    PersistencePort persistencePort;

    public ListUseCase(PersistencePort persistencePort) {
        this.persistencePort = persistencePort;
    }

    @Override
    public void execute(ListInput listInput, UseCasePresenter<List<Idea>> listUseCasePresenter) {
        try {
            listUseCasePresenter.onSuccess(persistencePort.listForCustomer(listInput.getCustomerId()));
        }catch (Exception e){
            listUseCasePresenter.onError(e);
        }
    }
}
