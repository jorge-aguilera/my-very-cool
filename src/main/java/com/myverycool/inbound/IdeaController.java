package com.myverycool.inbound;

import com.myverycool.core.model.Idea;
import com.myverycool.core.usecases.UseCasePresenter;
import com.myverycool.core.usecases.ideas.add.AddInput;
import com.myverycool.core.usecases.ideas.add.AddUseCase;
import com.myverycool.core.usecases.ideas.list.ListInput;
import com.myverycool.core.usecases.ideas.list.ListUseCase;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.authentication.Authentication;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Single;

import java.util.List;
import java.util.Map;

@Secured("isAuthenticated()")
@Controller("/ideas")
public class IdeaController {

    ListUseCase listUseCase;

    AddUseCase addUseCase;

    public IdeaController(ListUseCase listUseCase, AddUseCase addUseCase) {
        this.listUseCase = listUseCase;
        this.addUseCase = addUseCase;
    }

    @Get("/")
    Flowable<Idea>index(final Authentication authentication){
        return Flowable.create(emitter -> {
            String customerId = authentication.getAttributes().get("customerId").toString();
            listUseCase.execute(new ListInput(customerId), new UseCasePresenter<>() {
                @Override
                public void onSuccess(List<Idea> result) {
                    result.stream().forEach(item->emitter.onNext(item));
                    emitter.onComplete();
                }

                @Override
                public void onError(Throwable t) {
                    emitter.onError(t);
                }
            });
        }, BackpressureStrategy.BUFFER);
    }

    @Post("/")
    Single<Idea>create(final Authentication authentication, final Map<String,String> request){
        return Single.create(emitter -> {
            String customerId = authentication.getAttributes().get("customerId").toString();
            String customer = authentication.getAttributes().get("name").toString();
            addUseCase.execute(new AddInput(customerId, customer, request.remove("type"), request), new UseCasePresenter<>() {
                @Override
                public void onSuccess(Idea result) {
                    emitter.onSuccess(result);
                }

                @Override
                public void onError(Throwable t) {
                    emitter.onError(t);
                }
            });
        });
    }
}
