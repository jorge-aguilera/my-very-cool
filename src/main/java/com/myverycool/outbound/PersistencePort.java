package com.myverycool.outbound;


import com.myverycool.core.model.Idea;

import java.util.List;

public interface PersistencePort {

    List<Idea>listForCustomer(String customerId);
    Idea save(Idea idea);
}
