package com.myverycool.outbound.mongo;

import com.myverycool.core.model.Customer;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.time.Instant;
import java.util.List;
import java.util.Map;

public class IdeaEntity {

    @BsonProperty("_id")
    @BsonId
    public ObjectId _id;

    public Instant createdAt = Instant.now();

    public String customerId;

    public String customer;

    public String type;

    public List<Customer> shared;

    public Map<String, String> attributes = Map.of();

}
