package com.myverycool.outbound.mongo;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.InsertOneResult;
import com.myverycool.core.model.Customer;
import com.myverycool.core.model.Idea;
import com.myverycool.outbound.PersistencePort;
import org.bson.Document;

import javax.inject.Singleton;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import com.mongodb.client.model.Filters;
import org.bson.conversions.Bson;

@Singleton
public class MongoPersistenceService implements PersistencePort {

    MongoRepository repository;

    public MongoPersistenceService(MongoRepository repository) {
        this.repository = repository;
    }

    public List<Idea>listForCustomer(String customerId){
        Bson filter = Filters.or(
                Filters.eq("customerId", customerId),
                Filters.eq("shared._id", customerId)
        );
        return repository.findAll(filter)
                .stream()
                .map(item->documentToIdea(item))
                .collect(Collectors.toList());
    }

    public Idea save(Idea idea) {
        IdeaEntity document = ideaToDocument(idea);
        InsertOneResult result = repository.save( document );
        if( result.wasAcknowledged() == false){
            throw new RuntimeException("Error saving idea");
        }
        idea.id=result.getInsertedId().asObjectId().getValue().toHexString();
        return idea;
    }

    protected static IdeaEntity ideaToDocument(Idea idea){
        IdeaEntity ret = new IdeaEntity();
        ret.customerId = idea.author.id;
        ret.customer = idea.author.name;
        ret.type = idea.type;
        ret.attributes = idea.attributes;
        return ret;
    }

    protected static Idea documentToIdea(IdeaEntity document){
        Idea ret = new Idea();
        ret.id = document._id.toHexString();
        ret.author = new Customer();
        ret.author.id = document.customerId;
        ret.author.name = document.customer;
        ret.type = document.type;
        ret.attributes = document.attributes;
        ret.shared = document.shared;
        return ret;
    }
}
