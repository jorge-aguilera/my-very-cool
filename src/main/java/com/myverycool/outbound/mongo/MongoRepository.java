package com.myverycool.outbound.mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.result.InsertOneResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class MongoRepository {

    MongoClient mongoClient;

    protected MongoRepository(MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

    protected long count(){
        return getCollection().countDocuments();
    }

    protected void clean(){
        getCollection().deleteMany(new Document());
    }

    protected IdeaEntity findById( String id){
        BasicDBObject query = new BasicDBObject();
        query.put("_id", new ObjectId(id));
        return getCollection().find(query).first();
    }

    protected List<IdeaEntity> findAll(Bson query){
        List<IdeaEntity> ret = new ArrayList<>();
        for(IdeaEntity doc : getCollection().find(query)){
            ret.add(doc);
        }
        return ret;
    }

    protected InsertOneResult save(IdeaEntity entity){
        return getCollection().insertOne(entity);

    }

    protected MongoCollection<IdeaEntity> getCollection() {
        return mongoClient
                .getDatabase("main")
                .getCollection("items", IdeaEntity.class);
    }


}
