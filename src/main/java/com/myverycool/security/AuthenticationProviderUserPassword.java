package com.myverycool.security;

import io.micronaut.core.annotation.Nullable;
import io.micronaut.http.HttpRequest;
import io.micronaut.security.authentication.*;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import org.reactivestreams.Publisher;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Map;

@Singleton
public class AuthenticationProviderUserPassword implements AuthenticationProvider {

    Map<String, String> usersPasswords = Map.of(
            "dummy", "test",
            "test", "dummy"
    );

    Map<String, Map<String, Object>> customersDB = Map.of(
            "dummy",Map.of("customerId","c1", "name", "customer 1"),
            "test",Map.of("customerId","c2", "name", "customer 2")
    );

    @Override
    public Publisher<AuthenticationResponse> authenticate(@Nullable HttpRequest<?> httpRequest, AuthenticationRequest<?, ?> authenticationRequest) {
        return Flowable.create(emitter -> {
            String user =authenticationRequest.getIdentity().toString();
            String pwd =authenticationRequest.getSecret().toString();

            if ( usersPasswords.containsKey(user) && usersPasswords.get(user).equals(pwd)) {
                emitter.onNext(
                        new UserDetails(
                                (String) authenticationRequest.getIdentity(),
                                new ArrayList<>(),
                                customersDB.get(user)
                        )
                );
                emitter.onComplete();
            } else {
                emitter.onError(new AuthenticationException(new AuthenticationFailed()));
            }
        }, BackpressureStrategy.ERROR);
    }
}

