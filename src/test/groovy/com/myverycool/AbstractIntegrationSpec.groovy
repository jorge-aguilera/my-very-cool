package com.myverycool

import io.micronaut.configuration.mongo.core.MongoSettings
import io.micronaut.context.ApplicationContext
import io.micronaut.context.env.PropertySource
import io.micronaut.runtime.server.EmbeddedServer
import org.testcontainers.containers.MongoDBContainer
import org.testcontainers.containers.wait.strategy.Wait
import org.testcontainers.spock.Testcontainers
import org.testcontainers.utility.DockerImageName
import spock.lang.Shared
import spock.lang.Specification

@Testcontainers
abstract class AbstractIntegrationSpec extends Specification{

    @Shared
    EmbeddedServer embeddedServer

    @Shared
    ApplicationContext applicationContext

    @Shared
    MongoDBContainer mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo"))
            .withExposedPorts(27017)
            .waitingFor(Wait.forListeningPort())

    void setupSpec(){
        mongoDBContainer.start()
        runEmbeddedServer()
    }

    private void runEmbeddedServer(){
        embeddedServer =  ApplicationContext.run(
                EmbeddedServer,
                PropertySource.of("test", Map.of(
                        MongoSettings.MONGODB_URI,"mongodb://${mongoDBContainer.containerIpAddress}:${mongoDBContainer.getMappedPort(27017)}")
                )
        )
        applicationContext = embeddedServer.applicationContext
    }



}
