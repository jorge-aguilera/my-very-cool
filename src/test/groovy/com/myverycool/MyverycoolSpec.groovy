package com.myverycool

import com.mongodb.client.MongoClient

class MyverycoolSpec extends AbstractIntegrationSpec {

    void 'test it works'() {
        when:
        MongoClient mongoClient = applicationContext.getBean(MongoClient)

        and:
        def databases = mongoClient.listDatabaseNames().collect()

        then:
        databases.size()
        and:
        ["admin", "config", "local"] == databases
    }

}
