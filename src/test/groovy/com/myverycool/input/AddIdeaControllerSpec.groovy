package com.myverycool.input

import com.myverycool.AbstractIntegrationSpec
import com.myverycool.core.model.Idea
import com.myverycool.outbound.mongo.MongoRepository
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException
import org.bson.Document

class AddIdeaControllerSpec extends AbstractIntegrationSpec{

    void "add an idea store an idea into datastore"(){
        given:
        HttpClient client = applicationContext.createBean(HttpClient, embeddedServer.URL)

        and:
        def repository = applicationContext.getBean(MongoRepository)

        and:
        Map request = [ type: ' note', 'text':'blablabla']

        and:
        def login = client.toBlocking()
                .exchange(HttpRequest.POST("/login", [username:'dummy', password:'test']),Map).body()

        when:
        Idea stored = client.toBlocking()
                .exchange(HttpRequest.POST("/ideas", request).bearerAuth(login.access_token),Map).body()

        then:
        stored.id
        stored.attributes.text == 'blablabla'

        and:
        repository.count()==1
    }
}
