package com.myverycool.input

import com.myverycool.AbstractIntegrationSpec
import com.myverycool.core.model.Idea
import com.myverycool.outbound.mongo.IdeaEntity
import com.myverycool.outbound.mongo.MongoRepository
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException
import org.bson.Document

class ListIdeaControllerSpec extends AbstractIntegrationSpec{

    void "list ideas is accesible"(){
        given:
        HttpClient client = applicationContext.createBean(HttpClient, embeddedServer.URL)

        and:
        def repository = applicationContext.getBean(MongoRepository)

        and:
        IdeaEntity document1 = new IdeaEntity( attributes: [text:'text1'], customerId :'c1', 'customer':'test1')
        repository.save(document1)

        and:
        def login = client.toBlocking()
                .exchange(HttpRequest.POST("/login", [username:'dummy', password:'test']),Map).body()

        when:
        List<Idea> ideas = client.toBlocking()
                .retrieve(HttpRequest.GET("/ideas").bearerAuth(login.access_token), List<Idea>)

        then:
        ideas.size() == 1
    }

}
