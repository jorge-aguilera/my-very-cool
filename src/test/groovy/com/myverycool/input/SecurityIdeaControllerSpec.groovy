package com.myverycool.input

import com.myverycool.AbstractIntegrationSpec
import com.myverycool.core.model.Idea
import com.myverycool.outbound.mongo.MongoRepository
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException
import org.bson.Document

class SecurityIdeaControllerSpec extends AbstractIntegrationSpec{

    void "list ideas is protected"(){
        given:
        HttpClient client = applicationContext.createBean(HttpClient, embeddedServer.URL)

        when:
        client.toBlocking().retrieve("/ideas", Idea)

        then:
        HttpClientResponseException unauthorized = thrown()
    }

    void "list ideas is accesible"(){
        given:
        HttpClient client = applicationContext.createBean(HttpClient, embeddedServer.URL)

        and:
        def login = client.toBlocking()
                .exchange(HttpRequest.POST("/login", [username:'dummy', password:'test']),Map).body()

        when:
        List<Idea> ideas = client.toBlocking()
                .retrieve(HttpRequest.GET("/ideas").bearerAuth(login.access_token), List<Idea>)

        then:
        ideas.size() == 0
    }

    void "add ideas is protected"(){
        given:
        HttpClient client = applicationContext.createBean(HttpClient, embeddedServer.URL)

        when:
        client.toBlocking().retrieve(
                HttpRequest.POST("/ideas", new Idea(attributes: [text:'blablablab']))
        )

        then:
        HttpClientResponseException unauthorized = thrown()
    }

}
