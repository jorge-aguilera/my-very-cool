package com.myverycool.outbound.mongo

import com.myverycool.AbstractIntegrationSpec
import com.myverycool.core.model.Customer
import com.myverycool.core.model.Idea

class MongoPortSpec extends AbstractIntegrationSpec{

    void 'add a Note'(){
        given:
        MongoPersistenceService mongoPort = applicationContext.getBean(MongoPersistenceService)
        MongoRepository repository = applicationContext.getBean(MongoRepository)

        and:
        Idea note = new Idea( attributes: [text:'blablabla'], type: 'note', author: new Customer(id:'1',name:'test'))

        when:
        note = mongoPort.save(note)

        then:
        note.id

        and:
        repository.count() == 1

        and:
        repository.findById(note.id).attributes['text'] == 'blablabla'

        cleanup:
        repository.clean()
    }

    void 'list ideas'(){
        given:
        MongoPersistenceService mongoPort = applicationContext.getBean(MongoPersistenceService)
        MongoRepository repository = applicationContext.getBean(MongoRepository)

        and:
        IdeaEntity document1 = new IdeaEntity( attributes: [text:'text1'], customerId :'2', 'customer':'test1')
        repository.save(document1)
        IdeaEntity document2 = new IdeaEntity( attributes: [text:'text2'], customerId :'2', 'customer':'test1')
        repository.save(document2)
        IdeaEntity document3 = new IdeaEntity( attributes: [text:'text3'], customerId :'3', 'customer':'test2')
        repository.save(document3)

        when:
        List<Idea> ideas = mongoPort.listForCustomer('2')

        then:
        ideas.size() == 2
        ideas*.attributes.text==['text1','text2']

        cleanup:
        repository.clean()
    }

    void 'list shared ideas'(){
        given:
        MongoPersistenceService mongoPort = applicationContext.getBean(MongoPersistenceService)
        MongoRepository repository = applicationContext.getBean(MongoRepository)

        and:
        IdeaEntity document1 = new IdeaEntity( attributes: [text:'text1'], customerId :'4', 'customer':'test1')
        repository.save(document1)
        IdeaEntity document2 = new IdeaEntity( attributes: [text:'text2'], customerId :'4', 'customer':'test1')
        repository.save(document2)
        IdeaEntity document3 = new IdeaEntity( attributes: [text:'text3'], customerId :'5', 'customer':'test2',
                shared: [new Customer(id: '4', name:'test1')])
        repository.save(document3)

        when:
        List<Idea> ideas = mongoPort.listForCustomer('4')

        then:
        ideas.size() == 3
        ideas*.attributes.text==['text1','text2', 'text3']

        cleanup:
        repository.clean()
    }

}
