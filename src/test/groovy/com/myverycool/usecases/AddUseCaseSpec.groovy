package com.myverycool.usecases;

import com.myverycool.AbstractIntegrationSpec;
import com.myverycool.core.model.Idea
import com.myverycool.core.usecases.UseCasePresenter
import com.myverycool.core.usecases.ideas.add.AddInput
import com.myverycool.core.usecases.ideas.add.AddUseCase
import com.myverycool.outbound.mongo.MongoRepository;

class AddUseCaseSpec extends AbstractIntegrationSpec {

    AddUseCase addUseCase
    MongoRepository repository

    void setup(){
        addUseCase = applicationContext.getBean(AddUseCase)
        repository = applicationContext.getBean(MongoRepository)
    }


    void "customerId is required to add a simple idea"(){
        given:
        def attributes = [text:'blablabla']

        when:
        addUseCase.execute(new AddInput(null,null,null,attributes), new UseCasePresenter<Idea>() {
            @Override
            void onSuccess(Idea result) {
            }

            @Override
            void onError(Throwable t) {
                throw t;
            }
        })

        then:
        IllegalArgumentException ile = thrown()

        and:
        repository.count()==0
    }

    void "can add a simple idea"(){
        given:
        def attributes = [text:'blablabla']

        when:
        Idea idea
        addUseCase.execute(new AddInput('test',"name","note", attributes), new UseCasePresenter<Idea>() {
            @Override
            void onSuccess(Idea result) {
                idea = result
            }

            @Override
            void onError(Throwable t) {
                throw t;
            }
        })
        then:
        idea
        idea.id

        and:
        repository.count()==1
    }

}
