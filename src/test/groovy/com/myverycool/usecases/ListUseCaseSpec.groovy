package com.myverycool.usecases

import com.myverycool.AbstractIntegrationSpec
import com.myverycool.core.model.Idea
import com.myverycool.core.usecases.UseCasePresenter
import com.myverycool.core.usecases.ideas.add.AddInput
import com.myverycool.core.usecases.ideas.add.AddUseCase
import com.myverycool.core.usecases.ideas.list.ListInput
import com.myverycool.core.usecases.ideas.list.ListUseCase
import com.myverycool.outbound.mongo.IdeaEntity
import com.myverycool.outbound.mongo.MongoRepository
import io.reactivex.Flowable
import org.bson.Document

class ListUseCaseSpec extends AbstractIntegrationSpec {

    ListUseCase listUseCase
    MongoRepository repository

    void setup(){
        listUseCase = applicationContext.getBean(ListUseCase)
        repository = applicationContext.getBean(MongoRepository)
    }

    void "can retrieve a list of ideas from a customer"(){
        given:
        IdeaEntity document1 = new IdeaEntity( attributes: [text:'text1'], customerId :'1', 'customer':'test1')
        repository.save(document1)
        IdeaEntity document2 = new IdeaEntity( attributes: [text:'text1'], customerId :'1', 'customer':'test1')
        repository.save(document2)
        IdeaEntity document3 = new IdeaEntity( attributes: [text:'text1'], customerId :'2', 'customer':'test2')
        repository.save(document3)

        when:
        List<Idea> ideas
        listUseCase.execute( new ListInput('1'), new UseCasePresenter<List<Idea>>() {
            @Override
            void onSuccess(List<Idea> result) {
                ideas = result
            }

            @Override
            void onError(Throwable t) {
                throw t
            }
        })
        then:
        ideas.size()==2
    }

}
